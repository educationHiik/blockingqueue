
package blokingqueueexample;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class BlokingQueueExample
{
    public static void main(String[] args)
    {
        // Блокирующая очередь для передачи сообщений 
        BlockingQueue<Packet> queue = new LinkedBlockingQueue<>();
        System.out.println("Сформировал экземпляр класса блокирующей очереди {queue}");
     
        
        FXMLController controller = new FXMLController();
        System.out.println("Сформировал экземпляр класса {FXMLController}");
        controller.setQueue(queue);
        System.out.println("Экземпляр класса блокирующей очереди {queue}  передан в класс {FXMLController}");
        

        // Генератор сообщений {Packet} для бокирующей очереди
        PacketProcessor packetProcessor = new PacketProcessor();
        System.out.println("Сформировал экземпляр класса {packetProcessor}");
        // Передача экземпляра блокирующей очереди в экземпляр packetProcessor
        packetProcessor.setQueue(queue);
        System.out.println("Экземпляр класса блокирующей очереди {queue}  передан в класс {packetProcessor}");
        
        // Запуск блокирующей очереди в экземпляре controller для приема сообщений {Packet}
        controller.readQueue();
        
        // Генерация 10 пакетов {Packet} и передача для получения в controller
        packetProcessor.generatePacket();

    }

}
