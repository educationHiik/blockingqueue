/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blokingqueueexample;

import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vaganovdv
 */
public class PacketProcessor
{
    
    //         имя класса     тип данных     наименование экземпляра
    //           |            /              /
    private  BlockingQueue <Packet>         queue;
    
    
     
    public void setQueue(BlockingQueue <Packet> queue)
    {
        this.queue = queue;
        System.out.println("[PacketProcessor] получен экземпляр класса очереди");
    } 
    
    
    
    /**
     * Функция генерации пакетов - формирует список из 10  экземпляров  класса {Packet}
     * и размещает сформированные экземпляры в очереди
     * 
     */
    public void generatePacket()
    {
         for (int i = 0; i < 10; i++)
        {
            Packet packet = new Packet();
            int j = i+1;
            packet.setCommand("Команда {"+j+"}");
             try
             {
                queue.put(packet);
                System.out.println("[PacketProcessor.generatePacket] размещено в очереди: "+packet.getCommand()); 
             } catch (InterruptedException ex)
             {
                 Logger.getLogger(PacketProcessor.class.getName()).log(Level.SEVERE, null, ex);
             }
        }
    }        
    
}
