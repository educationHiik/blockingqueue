/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blokingqueueexample;

import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vaganovdv
 */
public class FXMLController
{
    // Блокирующая очередь для приема сообщений
    private  BlockingQueue <Packet> queue;
     
    
    //  Функция для установки блокирующей очереди 
    //  (экземплря блокирующей очереди формируется  внешним  BlokingQueueExample
    //
    
    public void setQueue(BlockingQueue <Packet> queue)
    {
        this.queue = queue;
        System.out.println("[FXMLController] получен экземпляр класса очереди");
    }
    
    
    // Метод для запуска нити, формирующей беспонечный цикл приема сообщений
    // из блокирующей очереди
    // 
    public void readQueue()
    {
        //  Формирование экземпляра Runnable, запускамого в отдельной нити
        //

         Runnable readQueue = new Runnable()
        {
            public void run()
            {
                // Бесконечный цикл получения пакетов из блокирующей очереди
                while (true)
                {
                    try
                    {
                        //  Извлечение пакета из блокирующей  очереди
                        Packet packet = queue.take();
                        if (packet != null)
                        {
                            // Вывод команды пакета на печать
                            System.out.println("[FXMLController] Получен пакет из очереди: {" + packet.getCommand() + "}");
                        }
                    } catch (InterruptedException ex)
                    {
                        Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
    
        // Формирование отдельной нити для получения пакетов из блокирующей очереди 
        //    Экземпляр нити           экземпляр класса {Runnable} -> readQueue
        //       |                      /
        Thread thread = new Thread(readQueue);
        thread.start();    
        System.out.println("[FXMLController] Запущена нить чтения из очереди");
    }        
    
    
}
