/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blokingqueueexample;

/**
 *  Эзкмепляр класса для передачи через блокирующую очередь
 * 
 * @author vaganovdv
 */
public class Packet
{

     private String command;
    
    /**
     * @return the command
     */
    public String getCommand()
    {
        return command;
    }

    /**
     * @param command the command to set
     */
    public void setCommand(String command)
    {
        this.command = command;
    }
   
}
